package Controller;

import Entities.Shape;
import Entities.ShapeFactory;

import java.util.InputMismatchException;
import java.util.Scanner;

public class Client {

  public static void main(String args[]) {
    ShapeFactory shapeFactory = new ShapeFactory();
    System.out.println("What do you want to create?\n1.Circle\n2.Triangle");
    try {
      Scanner scanner = new Scanner(System.in);
      int i = scanner.nextInt();
      Shape shapeInstance;
      if (i == 1) {
        System.out.println("Enter radius");
        scanner = new Scanner(System.in);
        double radius = scanner.nextDouble();
        //    new Circle(radius);                 Not Allowed
        shapeInstance = shapeFactory.getShapeInstance(radius);
        showOutput(shapeInstance);
      } else if (i == 2) {
        System.out.println("Enter 3 sides separated with spaces");
        scanner = new Scanner(System.in);
        double sideA = scanner.nextDouble();
        double sideB = scanner.nextDouble();
        double sideC = scanner.nextDouble();
        shapeInstance = shapeFactory.getShapeInstance(sideA, sideB, sideC);
        showOutput(shapeInstance);
      } else {
        System.out.println("Invalid input");
      }
    } catch (InputMismatchException e ) {
      System.out.println("Please enter valid input");
    }
  }

  private static void showOutput(Shape shapeInstance) {
    System.out.println("Name: " + shapeInstance.getName());
    System.out.println("Area: " + shapeInstance.getSurfaceArea());
    System.out.println("Perimeter: " + shapeInstance.getPerimeter());
  }
}
