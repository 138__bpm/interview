package Entities;

public class Circle extends Shape {

  private double radius;

  Circle(double radius) {
    setRadius(radius);
    setName();
  }

  @Override
  void setName() {
    this.name = "Circle";
  }

  @Override
  void setPerimeter() {
    this.perimeter = 2 * Math.PI * radius;
  }

  @Override
  void setSurfaceArea() {
    this.surfaceArea = Math.PI * radius * radius;
  }

  public void setRadius(double radius) {
    this.radius = radius;
    setPerimeter();
    setSurfaceArea();
  }

  public double getRadius() {
    return radius;
  }

}
