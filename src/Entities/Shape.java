package Entities;

public abstract class Shape {

  Shape() {

  }

  String name;

  double perimeter;

  double surfaceArea;

  public String getName() {
    return name;
  }

  public double getSurfaceArea() {
    return surfaceArea;
  }

  public double getPerimeter() {
    return perimeter;
  }

  abstract void setName();

  abstract void setPerimeter();

  abstract void setSurfaceArea();
}
