package Entities;

public class ShapeFactory {

  public Shape getShapeInstance (double radius) {
    return new Circle(radius);
  }

  public Shape getShapeInstance (double sideA, double sideB, double sideC) {
    return new Triangle(sideA, sideB, sideC);
  }
}
