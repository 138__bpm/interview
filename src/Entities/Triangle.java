package Entities;

public class Triangle extends Shape {

  private double sideA;
  private double sideB;
  private double sideC;

  Triangle(double sideA, double sideB, double sideC) {
    setSides(sideA, sideB, sideC);
    setName();
  }

  @Override
  void setName() {
    if(sideA == sideB && sideB == sideC) {
      this.name = "Equilateral Triangle";
    } else if (sideA == sideB || sideA == sideC || sideB == sideC) {
      this.name = "Isosceles Triangle";
    } else {
      this.name = "Scalene Triangle";
    }
  }

  @Override
  void setPerimeter() {
    this.perimeter = sideA + sideB + sideC;
  }

  @Override
  void setSurfaceArea() {
    double halfPerimeter = (sideA + sideB + sideC) / 2;
    double squaredArea = halfPerimeter * (halfPerimeter - sideA) * (halfPerimeter - sideB) * (halfPerimeter - sideC);
    this.surfaceArea = Math.sqrt(squaredArea);
  }

  public void setSides(double sideA, double sideB, double sideC) {
    this.sideA = sideA;
    this.sideB = sideB;
    this.sideC = sideC;
    setPerimeter();
    setSurfaceArea();
  }
}
